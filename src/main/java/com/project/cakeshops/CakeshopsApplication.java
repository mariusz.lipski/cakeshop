package com.project.cakeshops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CakeshopsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CakeshopsApplication.class, args);
    }

}
