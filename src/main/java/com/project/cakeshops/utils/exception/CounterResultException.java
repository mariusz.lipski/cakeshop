package com.project.cakeshops.utils.exception;

public class CounterResultException extends RuntimeException {

    public CounterResultException(String message) {
        super(message);
    }
}
