package com.project.cakeshops.utils.exception;

public class CSVGenerationIOException extends RuntimeException{
    public CSVGenerationIOException(String message) {
        super(message);
    }
}
