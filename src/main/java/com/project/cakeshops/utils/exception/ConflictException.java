package com.project.cakeshops.utils.exception;

public class ConflictException  extends RuntimeException{
    public ConflictException(String message) {
        super(message);
    }
}
