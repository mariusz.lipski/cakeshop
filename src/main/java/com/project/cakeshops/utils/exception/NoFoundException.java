package com.project.cakeshops.utils.exception;

public class NoFoundException extends RuntimeException{

    public NoFoundException(String message) {
        super(message);
    }
}
