package com.project.cakeshops.utils.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({
        SQLDatabaseException.class,
        CSVGenerationIOException.class,
        JsonException.class,
        CounterResultException.class
    })
    public ResponseEntity<ErrorResponse> generateInternalServerErrorException(Exception ex) {
        return ResponseEntity
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(ErrorResponse
                .builder()
                .message(ex.getMessage())
                .build());
    }

    @ExceptionHandler({
        NoFoundException.class
    })
    public ResponseEntity<ErrorResponse> generateNoFoundException(Exception ex) {
        return ResponseEntity
            .status(HttpStatus.NOT_FOUND)
            .body(ErrorResponse
                .builder()
                .message(ex.getMessage())
                .build());
    }

    @ExceptionHandler({
        BadRequestException.class,
        java.lang.IllegalArgumentException.class,
    })
    public ResponseEntity<ErrorResponse> generateBadRequestException(Exception ex) {
        return ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .body(ErrorResponse
                .builder()
                .message(ex.getMessage())
                .build());
    }

    @ExceptionHandler({
        NoAccessException.class,
    })
    public ResponseEntity<ErrorResponse> generateForbiddenException(Exception ex) {
        return ResponseEntity
            .status(HttpStatus.FORBIDDEN)
            .body(ErrorResponse
                .builder()
                .message(ex.getMessage())
                .build());
    }

    @ExceptionHandler({
            ConflictException.class,
    })
    public ResponseEntity<ErrorResponse> generateNoConflictException(Exception ex) {
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(ErrorResponse
                        .builder()
                        .message(ex.getMessage())
                        .build());
    }
}

