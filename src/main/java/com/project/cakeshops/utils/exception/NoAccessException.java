package com.project.cakeshops.utils.exception;

public class NoAccessException extends RuntimeException{

    public NoAccessException(String message) {
        super(message);
    }
}
