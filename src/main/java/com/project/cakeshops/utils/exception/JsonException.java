package com.project.cakeshops.utils.exception;

public class JsonException extends RuntimeException{

    public JsonException(String message) {
        super(message);
    }
}
