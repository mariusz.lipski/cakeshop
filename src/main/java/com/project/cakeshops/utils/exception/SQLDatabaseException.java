package com.project.cakeshops.utils.exception;

public class SQLDatabaseException extends RuntimeException{

    public SQLDatabaseException(String message) {
        super(message);
    }
}
