//package com.project.cakeshops.configuration;
//
//import liquibase.integration.spring.SpringLiquibase;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.sql.DataSource;
//
//@Configuration
//public class LiquibaseConfiguration {
//
//    @Value("${liquibase.changelog}")
//    String liquibaseChangelog;
//
//    @Bean
//    public SpringLiquibase liquibase(@Qualifier("esealsDataSource") DataSource dataSource) {
//        SpringLiquibase liquibase = new SpringLiquibase();
//        liquibase.setDataSource(dataSource);
//        liquibase.setChangeLog(liquibaseChangelog);
//        return liquibase;
//    }
//}
