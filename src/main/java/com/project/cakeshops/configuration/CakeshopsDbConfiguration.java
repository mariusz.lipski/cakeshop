package com.project.cakeshops.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    entityManagerFactoryRef = "cakeshopsManagerFactory",
    transactionManagerRef = "cakeshopsTransactionManager",
    basePackages = {
        "com.project.cakeshops.*"
    })
public class CakeshopsDbConfiguration {

    @Value("${cakeshops.database.url}")
    private String dbUrl;

    @Value("${cakeshops.database.username}")
    private String dbUsername;

    @Value("${cakeshops.database.password}")
    private String dbPassword;

    @Primary
    @Bean(name = "cakeshopsDataSource")
    public DataSource cakeshopsDbDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);
        return dataSource;
    }

    @Primary
    @Bean(name = "cakeshopsJdbcTemplate")
    public JdbcTemplate cakeshopsJdbcTemplate(@Qualifier("cakeshopsDataSource") DataSource cakeshops) {
        return new JdbcTemplate(cakeshops);
    }

    @Primary
    @Bean(name = "cakeshopsNamedParameterJdbcTemplate")
    public NamedParameterJdbcTemplate cakeshopsNamedParameterJdbcTemplate(@Qualifier("cakeshopsDataSource") DataSource cakeshops) {
        return new NamedParameterJdbcTemplate(cakeshops);
    }

    @Primary
    @Bean(name = "cakeshopsManagerFactory")
    public LocalContainerEntityManagerFactoryBean cakeshopsManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(cakeshopsDbDataSource());
        factoryBean.setPersistenceUnitName("cakeshopsManagerFactory");
        factoryBean.setPackagesToScan("com.project.cakeshops.command");
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setJpaProperties(setProperty());
        return factoryBean;
    }

    private Properties setProperty() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        properties.setProperty("hibernate.jdbc.time_zone", "UTC");
        properties.setProperty("spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation", "true");
        return properties;
    }

    @Primary
    @Bean(name = "cakeshopsTransactionManager")
    public PlatformTransactionManager cakeshopsTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(cakeshopsManagerFactory().getObject());
        return transactionManager;
    }
}
