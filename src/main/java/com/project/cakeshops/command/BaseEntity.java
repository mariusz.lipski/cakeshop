package com.project.cakeshops.command;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "base_entity")
public class BaseEntity {

    @Id
    @GeneratedValue(generator = "base_entity_id_seq")
    private Long id;

    @Column(name = "name")
    private String name;
}
