package com.project.cakeshops.command.cake;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "cake")
public class CakeEntity {
    @Id
    @GeneratedValue(generator = "base_entity_id_seq")
    private Long id;

    @Column(name = "name")
    private String name;
}
